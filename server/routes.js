/**
 * Created by hanni on 11/25/16.
 */
// exports.routes = function routes (app, secureRouters) { // For token
exports.routes = function routes (app, secureRouters) {

    var users = require('./components/users');
    var messages = require('./components/messages');

    // app.post('/signin', users.authenticationUser);     // app.  // For token
    secureRouters.post('/messages', messages.enterInChat);


//     app.get('/products', products.getAllProducts);
//     app.get('/products/:id', products.getProductById);
//     app.post('/products', products.addNewProduct);
//     app.put('/products/:id', products.updateProduct);
//     app.delete('/products/:id', products.deleteProduct); /secure-api/posts
};