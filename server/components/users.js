/**
 * Created by hanni on 11/25/16.
 */
var MongoClient = require('mongodb').MongoClient;
var jwt = require('jsonwebtoken');
process.env.SECRET_KEY = 'nikitkasecretkey';

exports.authenticationUser = function authenticationUser(req, res) {
    if(!req.body.login) {
        res.status(401).send('Login is mandatory');
        return null;
    }
    if(!req.body.password) {
        res.status(401).send('Password is mandatory');
        return null;
    }

    MongoClient.connect('mongodb://localhost:27017/users')
        .then(response => {
            var user = response.collection('users').find({login: req.body.login, password: req.body.password}).toArray();
            return user;
        })
        .then (user => {
            if(user.length < 1) {

            }

            var userToken = {
                _id: user[0]._id,
                name:user[0].login
            };
            var token = jwt.sign(userToken, process.env.SECRET_KEY, {
                expiresIn: 12000
            });

            res.json({
                success:true,
                token:token
            });
        });

};

exports.checkInUser = function checkInUser(req, res) {
    if (!req.body.login) {
        res.status(401).send('Login is mandatory');
    }
    if (!req.body.password) {
        res.status(401).send('Password is mandatory');
    }
    if (!req.body.name) {
        res.status(401).send('Name is mandatory');
    }

    MongoClient.connect('mongodb://localhost:27017/users')
        .then(response => {
            response.collection('users').insert({name: req.body.name, login: req.body.login, password: req.body.password});
            return response;
        })
        .then(response => {
            send('User added');
        });
};